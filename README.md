# Prérequis d'installation

IMPORTANT: Veuillez utiliser la dernière version de Node et en particulier NPM, pour vous assurer que le package-lock.json soit utilisé.

Pour exécuter ce projet, npm doit être installé sur votre machine. Voici quelques tutoriels pour installer node sur différents environnements :

*Il est important d'installer une version récente de node*

- [Installer Node et NPM sur Windows](https://www.youtube.com/watch?v=8ODS6RM6x7g)
- [Installer Node et NPM sur Linux](https://www.youtube.com/watch?v=yUdHk-Dk_BY)
- [Installer Node et NPM sur Mac](https://www.youtube.com/watch?v=Imj8PgG3bZU)


# Installation d'Angular CLI

Vous pouvez installer angular-cli de manière globale sur votre machine avec la commande suivante:

    npm install -g @angular/cli 


# Comment utiliser ce dépôt
 
Lancez la commande suivante pour installer les dépendances (node_modules) :

    npm install 

# Lancer le backend

Afin de pouvoir fournir des exemples réalistes, nous allons avoir besoin de faire tourner une petite API REST d'exemple comme backend. Vous pouvez lancer le backend d'exemple fourni avec la commande suivante :

    npm run server

Il s'agit d'une API REST servie par Express (serveur Node).
