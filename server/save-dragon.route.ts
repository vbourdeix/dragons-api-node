import {Request, Response} from 'express';
import {DRAGONS} from "./db-data";
import {setTimeout} from 'timers';


export function saveDragon(req: Request, res: Response) {

    const id = req.params["id"],
        changes = req.body;

    console.log("Saving dragon", id, JSON.stringify(changes));


    DRAGONS[id] = {
        ...DRAGONS[id],
        ...changes
    };

    setTimeout(() => {

        res.status(200).json(DRAGONS[id]);

    }, 2000);



}
