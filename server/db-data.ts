

export const DRAGONS: any = {
  "@context": "/api/contexts/Dragon",
  "@id": "/api/dragons",
  "@type": "hydra:Collection",
  "hydra:member": [
    {
      "@id": "/api/dragons/1366",
      "@type": "Dragon",
      "id": 1366,
      "name": "Shenron",
      "size": 20,
      "color": "jade",
      "power": "earth",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1367",
      "@type": "Dragon",
      "id": 1367,
      "name": "Spyro",
      "size": 30,
      "color": "cobra",
      "power": "fire",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1368",
      "@type": "Dragon",
      "id": 1368,
      "name": "Ignir",
      "size": 76,
      "color": "ruby",
      "power": "fire",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1369",
      "@type": "Dragon",
      "id": 1369,
      "name": "Coeur de Givre",
      "size": 55,
      "color": "diamond",
      "power": "ice",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1370",
      "@type": "Dragon",
      "id": 1370,
      "name": "Bahamut",
      "size": 20,
      "color": "electric blue",
      "power": "holy",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1371",
      "@type": "Dragon",
      "id": 1371,
      "name": "Voldemort",
      "size": 20,
      "color": "black",
      "power": "void",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1372",
      "@type": "Dragon",
      "id": 1372,
      "name": "Cobra",
      "size": 32,
      "color": "cobra",
      "power": "nature",
      "lifePoints": 100,
      "isLegendary": false
    },
    {
      "@id": "/api/dragons/1373",
      "@type": "Dragon",
      "id": 1373,
      "name": "Acnologia",
      "size": 99,
      "color": "sapphire",
      "power": "arcana",
      "lifePoints": 100,
      "isLegendary": false
    }
  ],
  "hydra:totalItems": 8
};

export function findDragonById(dragonId:number) {
    return DRAGONS[dragonId];
}

