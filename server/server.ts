
import * as express from 'express';
import {Application} from "express";
import {getAllDragons, getDragonById} from "./get-dragons.route";
import {saveDragon} from './save-dragon.route';

const bodyParser = require('body-parser');

const app: Application = express();

app.use(bodyParser.json());

app.route('/api/dragons').get(getAllDragons);

app.route('/api/dragons/:id').get(getDragonById);

app.route('/api/dragons/:id').put(saveDragon);



const httpServer = app.listen(9000, () => {
    console.log("HTTP REST API Server running at http://localhost:" + httpServer.address().port);
});



