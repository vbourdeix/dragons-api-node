

import {Request, Response} from 'express';
import {DRAGONS} from "./db-data";



export function getAllDragons(req: Request, res: Response) {

  setTimeout(() => {

    res.status(200).json({payload:Object.values(DRAGONS)});

  }, 200);
}


export function getDragonById(req: Request, res: Response) {

    const dragonId = req.params["id"];

    const dragons:any = Object.values(DRAGONS);

    const dragon = dragons.find(dragon => dragon.id == dragonId);

    res.status(200).json(dragon);
}
